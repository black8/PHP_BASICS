<?php

function death ($vDeath)
{
	if(is_array($vDeath))
	{
		echo '<pre>';
		print_r ($_POST);
		echo '</pre>';
	}
	else
	{
		var_dump($vDeath);
	}
}


//death ($_POST);

?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Simple Death certificate Form</title>
</head>
<body>
	<form action="index1.php" method="post">
		<section>
			<h1>Appendix A-Simple US Death Certificate Form</h1>
			<p>The sample death reporting form included in this content profile reflects much of the data captured for the U.S Standerd Certifcate of Death. However, the VRDR content profile may be modified to include and accommoate international death reporting requirements</p>
			<h2>Death Reporting For Vital Recordes</h2>
			<fieldset>
				<legend>Decedant's Name (Include AKA's if any)</legend>
				<ul>
					<li>
						<label for="Lastname">Last Name : </label>
						<?php if(is_array($_POST) && !empty($_POST['LastName']))
						{
							echo "<font color='green'>".$_POST['LastName']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="FirstName">  First Name : </label>
						<?php if(is_array($_POST) && !empty($_POST['FirstName']))
						{
							echo "<font color='green'>".$_POST['FirstName']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="MiddleName">  Middle Name : </label>
						<?php if(is_array($_POST) && !empty($_POST['MiddleName']))
						{
							echo "<font color='green'>".$_POST['MiddleName']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
					<li>
						<label for="Date_of_birth">Date of Birth</label>
						<?php if(is_array($_POST) && !empty($_POST['Date_of_birth']))
						{
							echo "<font color='green'>".$_POST['Date_of_birth']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Gender">Gender</label>
						<?php if(is_array($_POST) && !empty($_POST['origin']))
						{
							echo "<font color='green'>".$_POST['origin']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Social_Security_Number">Social Security Number</label>
						<?php if(is_array($_POST) && !empty($_POST['Social_Security_Number']))
						{
							echo "<font color='green'>".$_POST['Social_Security_Number']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="FacilityName">Facility Name</label>
						<?php if(is_array($_POST) && !empty($_POST['FacilityName']))
						{
							echo "<font color='green'>".$_POST['FacilityName']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
					</li>
				</ul>
			</fieldset>	
		</section>
		
		
		<section>
			<h1>Decedent of Hispanic Origin</h1>
			<fieldset>
				<h4>Check the box that best describes whether the desedent is Spanish / Hispanic / latino. Check the "NO" box if decedent is not Spanish / Hispanic / latino</h4>
				<ul>
					<li>
						<?php if(is_array($_POST) && !empty($_POST['Hispanic_Origin']))
						{
							echo "<font color='green'>".$_POST['Hispanic_Origin']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
					
				</ul>
			</fieldset>	
		</section>
		
		<section>
			<h1>Decedent's race</h1>
			<fieldset>
				<h4>Check one or more races to indicate what the Decedent considered himself or herself to be : </h4>
				<ul>
					<li>
						<?php if(is_array($_POST) && !empty($_POST['Decendent_Race']))
						{
							echo "<font color='green'>".$_POST['Decendent_Race']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
					</li>
					
				</ul>
			</fieldset>	
		</section>
		
		
		<section>
			<h1>Items Must be Completed by Person Who Pronunces or Certifies Death</h1>
			<fieldset>
				<ul>
					<li>
						<label for="Date_Pronunced_Dead">Date Pronunced Dead : </label>
						<?php if(is_array($_POST) && !empty($_POST['Date_Pronunced_Dead']))
						{
							echo "<font color='green'>".$_POST['Date_Pronunced_Dead']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Time_Pronunced_Dead">Time Pronunced Dead : </label>
						<?php if(is_array($_POST) && !empty($_POST['Time_Pronunced_Dead']))
						{
							echo "<font color='green'>".$_POST['Time_Pronunced_Dead']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Signature_of_Persion_Pronuncing_Death">Signature of Persion Pronuncing Death : </label>
						<?php if(is_array($_POST) && !empty($_POST['Signature_of_Persion_Pronuncing_Death']))
						{
							echo "<font color='green'>".$_POST['Signature_of_Persion_Pronuncing_Death']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
					<li>
						<label for="License_number">License number : </label>
						<?php if(is_array($_POST) && !empty($_POST['License_number']))
						{
							echo "<font color='green'>".$_POST['License_number']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Date_Sigend">Date Signed : </label>
						<?php if(is_array($_POST) && !empty($_POST['Date_Sigend']))
						{
							echo "<font color='green'>".$_POST['Date_Sigend']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Actual_or_Presumed_Date_of_Birth">Actual or Presumed Date of Birth : </label>
						<?php if(is_array($_POST) && !empty($_POST['Actual_or_Presumed_Date_of_Birth']))
						{
							echo "<font color='green'>".$_POST['Actual_or_Presumed_Date_of_Birth']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
					<li>
						<label for="Actual_or_Presumed_Time_of_Birth">Actual or Presumed Time of Birth : </label>
						<?php if(is_array($_POST) && !empty($_POST['Actual_or_Presumed_Time_of_Birth']))
						{
							echo "<font color='green'>".$_POST['Actual_or_Presumed_Time_of_Birth']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="WME">Was Medical Examiner Or Coroner Contacted?</label>
						<?php if(is_array($_POST) && !empty($_POST['Was_Medical_Examiner_or_Coroner_Contacted']))
						{
							echo "<font color='green'>".$_POST['Was_Medical_Examiner_or_Coroner_Contacted']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
					
				</ul>
			</fieldset>	
		</section>
		<section>
			<h1>Cause Of Death</h1>
			<fieldset>
				<legend>See Instractions and examples</legend>
				<p><b>PART 1. </b>Enter the chain of events -- diseases, injuries or complications -- that directly caused the death. DO NOT enter termnal events such as Approximateinterval cardiac arrest, respiratory arrest , or ventricular fibrillation without showing the etiology. DO NOT ABBREVIATE. Enter only one cause on a line.</p>
				<ul>
					<li>
						<label for="Immediate_cause">A.Immediate Cause (Final disease or condition resulting in death) : </label>
						<?php if(is_array($_POST) && !empty($_POST['Immediate_cause']))
						{
							echo "<font color='green'>".$_POST['Immediate_cause']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Due_TO">Due to (or as a consequnce of) : </label>
						<?php if(is_array($_POST) && !empty($_POST['Due_TO']))
						{
							echo "<font color='green'>".$_POST['Due_TO']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Onset_death">Onset to death : </label>
						<?php if(is_array($_POST) && !empty($_POST['Onset_death']))
						{
							echo "<font color='green'>".$_POST['Onset_death']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
					<li>
						<label for="Sequentially">B. Sequentially List conditions, (if any, leading to cause listed on line a.) : </label>
						<?php if(is_array($_POST) && !empty($_POST['Sequentially']))
						{
							echo "<font color='green'>".$_POST['Sequentially']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Due_TO2">Due to (or as a consequnce of) : </label>
						<?php if(is_array($_POST) && !empty($_POST['Due_TO2']))
						{
							echo "<font color='green'>".$_POST['Due_TO2']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Onset_death2">Onset to death : </label>
						<?php if(is_array($_POST) && !empty($_POST['Onset_death2']))
						{
							echo "<font color='green'>".$_POST['Onset_death2']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
					<li>
						<label for="Underlying">B. Enter the <b>Underlying Cause</b>,(disease or injury that initiated the events) : </label>
						<?php if(is_array($_POST) && !empty($_POST['Underlying']))
						{
							echo "<font color='green'>".$_POST['Underlying']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Due_TO3">Due to (or as a consequnce of) : </label>
						<?php if(is_array($_POST) && !empty($_POST['Due_TO3']))
						{
							echo "<font color='green'>".$_POST['Due_TO3']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Onset_death3">Onset to death : </label>
						<?php if(is_array($_POST) && !empty($_POST['Onset_death3']))
						{
							echo "<font color='green'>".$_POST['Onset_death3']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
					<li>
						<label for="Underlying4">Last : </label>
						<?php if(is_array($_POST) && !empty($_POST['Underlying4']))
						{
							echo "<font color='green'>".$_POST['Underlying4']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Onset_death4">Onset to death : </label>
						<?php if(is_array($_POST) && !empty($_POST['Onset_death4']))
						{
							echo "<font color='green'>".$_POST['Onset_death4']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br><br><br><br></label>
					</li>
					<li>
						<label for="PART2"><b>PART 2. </b>Enter other significiant conditions contributing to death but not resulting in the underlying cause giveb in PART 1</label>
						<label><br></label>
					</li>
					<li>
						<textarea name="part2" id="" cols="100" rows="7">
							<?php if(is_array($_POST) && !empty($_POST['part2']))
						{
							echo $_POST['part2'];
						}
						else
						{
							echo "Not Defined";
						}
						?>
						</textarea>
						<label><br><br><br></label>
					</li>
					
					<li>
						<label for="performed">Was An Autospy Performed?  </label>
						<?php if(is_array($_POST) && !empty($_POST['performed']))
						{
							echo "<font color='green'>".$_POST['performed']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?><br>
						<label for="Were_Autosp">Were Autospy Finding avilable to complete the Cause Of Death ? </label>
						<?php if(is_array($_POST) && !empty($_POST['Were_Autosp']))
						{
							echo "<font color='green'>".$_POST['Were_Autosp']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?><br>
						<label for="Tobacco">Did Tobacco Use Contribute to death </label>
						<?php if(is_array($_POST) && !empty($_POST['Tobacco']))
						{
							echo "<font color='green'>".$_POST['Tobacco']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
				</ul>
			</fieldset>	
		</section>
		
		<section>
			<h1>If Female</h1>
			<fieldset>
				<ul>
					<li>
						<?php if(is_array($_POST) && !empty($_POST['If_Famale']))
						{
							echo "<font color='green'>".$_POST['If_Famale']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
					</li>
				</ul>
			</fieldset>	
		</section>
		
		<section>
			<h1>Manner of Death</h1>
			<fieldset>
				<ul>
					<li>
						<?php if(is_array($_POST) && !empty($_POST['Death_Manner']))
						{
							echo "<font color='green'>".$_POST['Death_Manner']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
				</ul>
			</fieldset>	
		</section>
		
		<section>
			<h1>Injury</h1>
			<fieldset>
				<ul>
					<li>
						
						<label for="Date_of_injury"> Date Of Injury</label>
						<?php if(is_array($_POST) && !empty($_POST['Date_of_injury']))
						{
							echo "<font color='green'>".$_POST['Date_of_injury']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Time_of_injury">Time of Injury</label>
						<?php if(is_array($_POST) && !empty($_POST['Time_of_injury']))
						{
							echo "<font color='green'>".$_POST['Time_of_injury']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Place_of_injury">Place of Injury</label>
						<?php if(is_array($_POST) && !empty($_POST['Place_of_injury']))
						{
							echo "<font color='green'>".$_POST['Place_of_injury']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Injury_at_work">Injury At Work</label>
						<?php if(is_array($_POST) && !empty($_POST['Injury_at_work']))
						{
							echo "<font color='green'>".$_POST['Injury_at_work']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
				</ul>
			</fieldset>	
		</section>
		<section>
			<h1>Location Of Injury</h1>
			<fieldset>
				<ul>
					<li>
						
						<label for="State">State</label>
						<?php if(is_array($_POST) && !empty($_POST['State']))
						{
							echo "<font color='green'>".$_POST['State']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="City_or_town">City Or Town</label>
						<?php if(is_array($_POST) && !empty($_POST['City_or_town']))
						{
							echo "<font color='green'>".$_POST['City_or_town']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
					<li>
						
						<label for="Street_and_number">Street and Number</label>
						<?php if(is_array($_POST) && !empty($_POST['Street_and_number']))
						{
							echo "<font color='green'>".$_POST['Street_and_number']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Appartment_NO">Appartment NO</label>
						<?php if(is_array($_POST) && !empty($_POST['Appartment_NO']))
						{
							echo "<font color='green'>".$_POST['Appartment_NO']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Zip_Code">Zip Code</label>
						<?php if(is_array($_POST) && !empty($_POST['Zip_Code']))
						{
							echo "<font color='green'>".$_POST['Zip_Code']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
					<li>
						<label for="">Describe How Injury Occurred</label>
					</li>
					<li>
						<textarea name="injury_description" id="" cols="100" rows="7">
							<?php if(is_array($_POST) && !empty($_POST['injury_description']))
						{
							echo $_POST['injury_description'];
						}
						else
						{
							echo "Not Defined";
						}
						?>
						</textarea>
					</li>
				</ul>
			</fieldset>	
		</section>
		
		
		<section>
			<h1>If Transportation Injury</h1>
			<fieldset>
				<legend>Specify</legend>
				<ul>
					<li>
						<?php if(is_array($_POST) && !empty($_POST['Specify']))
						{
							echo "<font color='green'>".$_POST['Specify']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<input type="radio"   value="Driver" /> Other (Specify)
						<?php if(is_array($_POST) && !empty($_POST['other_specify']))
						{
							echo "<font color='green'>".$_POST['other_specify']."</font>";
						}
						else
						{
							echo "<input type='text' />";
						}
						?>
						<label><br><br></label>
					</li>
				</ul>
			</fieldset>	
		</section>
		
		<section>
			<h1>Certifier</h1>
			<fieldset>
				<legend>Check Only One</legend>
				<ul>
					<li>
						<?php if(is_array($_POST) && !empty($_POST['certifier']))
						{
							echo "<font color='green'>".$_POST['certifier']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
				</ul>
			</fieldset>	
		</section>
		
		<section>
			<h1></h1>
			<fieldset>
				<hr>
				<ul>
					<li>
						<label for="completing_name">Name : </label>
						<?php if(is_array($_POST) && !empty($_POST['completing_name']))
						{
							echo "<font color='green'>".$_POST['completing_name']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="completing_address">Address : </label>
						<?php if(is_array($_POST) && !empty($_POST['completing_address']))
						{
							echo "<font color='green'>".$_POST['completing_address']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="completing_zip_code">Zip Code : </label>
						<?php if(is_array($_POST) && !empty($_POST['completing_zip_code']))
						{
							echo "<font color='green'>".$_POST['completing_zip_code']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
					<li>
						<label for="time_of_certifier">Time Of Certifier </label>
						<?php if(is_array($_POST) && !empty($_POST['completing_zip_code']))
						{
							echo "<font color='green'>".$_POST['completing_zip_code']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="license_number">license number : </label>
						<?php if(is_array($_POST) && !empty($_POST['license_number']))
						{
							echo "<font color='green'>".$_POST['license_number']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label for="Date_certified">Date Certified : </label>
						<?php if(is_array($_POST) && !empty($_POST['Date_certified']))
						{
							echo "<font color='green'>".$_POST['Date_certified']."</font>";
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						<label><br><br></label>
					</li>
				</ul>
			</fieldset>	
		</section>
		<hr />
			<center><h3>Copyright &copy;2015 Shamem Ahmad</h3></center>
		<hr />
		<center>
			<input type="submit" value="Submit" />
			<input type="reset" value="Reset" />
		</center>
	</form>
</body>
</html>