<?php
session_start();
?>
<ul>
		<li><a href="ss.html">Add New</a></li>
		<li><a href="clear_session.php">Clear Session</a></li>
</ul> <hr />
<section>
<fieldset>
    <legend><h4>Decedent's Name (Include AKA's if any)</h4></legend>
    <table>
        <tr>
            <th>Serial</th>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Last Name</th>
            <th>Gender</th>
            <th>Social Security Number</th>
            <th>Facility Name</th>
            <th>Action</th>
        </tr>
        <?php
        $counter=1;
        foreach ($_SESSION['formdata'] as $death) 
		{
            ?>
            <tr>
                <td><?php echo $counter++; ?></td>
                <td>
					<?php if($death['FirstName'])
						{
							echo $death['FirstName'];
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
					?>
				</td>
                <td>
					<?php if($death['MiddleName'])
						{
							echo $death['MiddleName'];
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
					?>
				</td>
                <td>
					<?php if($death['LastName'])
						{
							echo $death['LastName'];
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
					?>
				</td>
                <td>
					<?php if($death['origin'])
						{
							echo $death['origin'];
						}
						else
						{
							echo "<font color='red'>Gender Not Defined</font>";
						}
					?>
				</td>
                <td>
					<?php if($death['Social_Security_Number'])
						{
							echo $death['Social_Security_Number'];
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
					?>
				</td>
                <td>
				<?php if($death['FacilityName'])
						{
							echo $death['FacilityName'];
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
				?>
                <td>
                    <a href="delete.php">Delete</a>
                    <a href="edit.php">Edit</a>
                    
                </td>
            </tr>
        <?php
        }
        ?>
    </table>
</fieldset>
</section>
		<section>
			<h1>Decedent of Hispanic Origin</h1>
			<fieldset>
				<h4>Check the box that best describes whether the desedent is Spanish / Hispanic / latino. Check the "NO" box if decedent is not Spanish / Hispanic / latino</h4>
				<table>
				<tr>
					<th>Serial</th>
					<th>Decedent of Hispanic Origin</th>
					<th>Action</th>
				</tr>
				
				<tr>
				<?php
						$counter=1;
						foreach ($_SESSION['formdata'] as $death) 
						{
				?>
					<td><?php echo $counter++ ?></td>
					<td>
						<?php if($death['Hispanic_Origin'])
						{
							echo $death['Hispanic_Origin'];
						}
						else
						{
							echo "<font color='red'>Hispanic Origin Not Defined</font>";
						}
				?>
					</td>
					
					<td>
						<a href="delete.php">Delete</a>
						<a href="edit.php">Edit</a>
					</td>
				</tr>
						<?php } ?>
				</table>
			</fieldset>	
</section>



<section>
			<h1>Decedent's race</h1>
			<fieldset>
				<h4>Check one or more races to indicate what the Decedent considered himself or herself to be : </h4>
				<table>
				
					<tr>
						<th>Serial</th>
						<th>Decedent's race</th>
						<th>Action</th>
					</tr>
					
					<?php
						$counter=1;
						foreach ($_SESSION['formdata'] as $death) 
						{
					?>
					<tr>
					<td><?php echo $counter++ ?></td>
					<td>
					<?php if($death['Decendent_Race'])
						{
							echo $death['Decendent_Race'];
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
				?>
					</td>
					<td>
						<a href="delete.php">Delete</a>
						<a href="edit.php">Edit</a>
					</td>
					</tr>
						<?php } ?>
				</table>
			</fieldset>	
		</section>
		
		
		<section>
			<h1>Items Must be Completed by Person Who Pronunces or Certifies Death</h1>
			<fieldset>
				<table>
					<tr>
						<th>Serial</th>
						<th>Date Pronunced Dead</th>
						<th>Time Pronunced Dead </th>
						<th>Signature of Persion Pronuncing Death</th>
						<th>License number</th>
						<th>Date Signed</th>
						<th>Actual or Presumed Date of Birth</th>
						<th>Actual or Presumed Time of Birth</th>
						<th>Was Medical Examiner Or Coroner Contacted?</th>
						<th>Action</th>
					</tr>
					
					<?php
						$counter=1;
						foreach ($_SESSION['formdata'] as $death) 
						{
							
					?><tr>
						<td><?php echo $counter++ ?></td>
						<td>
							<?php if($death['Date_Pronunced_Dead'])
						{
							echo $death['Date_Pronunced_Dead'];
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						</td>
						<td>
							<?php if($death['Time_Pronunced_Dead'])
						{
							echo $death['Time_Pronunced_Dead'];
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						</td>
						<td>
							<?php if($death['Signature_of_Persion_Pronuncing_Death'])
						{
							echo $death['Signature_of_Persion_Pronuncing_Death'];
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						</td>
						<td>
							<?php if($death['License_number'])
						{
							echo $death['License_number'];
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						</td>
						<td>
							<?php if($death['Date_Sigend'])
						{
							echo $death['Date_Sigend'];
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						</td>
						<td>
							<?php if($death['Actual_or_Presumed_Date_of_Birth'])
						{
							echo $death['Actual_or_Presumed_Date_of_Birth'];
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						</td>
						<td>
							<?php if($death['Actual_or_Presumed_Time_of_Birth'])
						{
							echo $death['Actual_or_Presumed_Time_of_Birth'];
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						</td>
						<td>
							<?php if($death['Was_Medical_Examiner_or_Coroner_Contacted'])
						{
							echo $death['Was_Medical_Examiner_or_Coroner_Contacted'];
						}
						else
						{
							echo "<font color='red'>Not Defined</font>";
						}
						?>
						</td>
						<td>
							<a href="delete.php">Delete</a>
							<a href="edit.php">Edit</a>
						</td>
						</tr>
						<?php } ?>
					
				</table>
			</fieldset>	
		</section>
		