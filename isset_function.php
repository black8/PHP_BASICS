<?php
$var='2';

if (isset($var))
{
	echo 'There is a value';
}
$a = "test";
$b = "anothertest";

var_dump(isset($a));      // TRUE
var_dump(isset($a, $b)); // TRUE

unset ($a);

var_dump(isset($a));     // FALSE
?>